/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"context"
	"fmt"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/exec"
)

type config struct {
	Host   string          `config:"host"`
	Port   string          `config:"port"`
	Src    string          `config:"source"`
	Mode   string          `config:"mode"`
	TlsCrt string          `config:"tlscrt"`
	TlsKey string          `config:"tlskey"`
	Hosts  map[string]Host `config:"hosts"`
}

type Host struct {
	Domain string
	Target string
}

var cfg = config{}

func main() {
	if len(os.Args) == 1 {
		fmt.Println("ERROR: You must specify a config file.")
		os.Exit(1)
	}

	for i := 1; i < len(os.Args); i++ {
		fileExist(os.Args[i])
	}

	if len(os.Args) > 2 {
		for i := 2; i < len(os.Args); i++ {
			c := exec.Command(os.Args[0], os.Args[i])
			go c.Run()
		}
	}

	var loader *confita.Loader
	if _, err := os.Stat(os.Args[1]); err == nil {
		loader = confita.NewLoader(
			file.NewBackend(os.Args[1]),
		)
	}
	err := loader.Load(context.Background(), &cfg)
	if err != nil {
		panic(err)
	}
	if cfg.Mode == "http" {
		http.Handle("/", http.FileServer(http.Dir(cfg.Src)))
		if err := http.ListenAndServe(cfg.Host+":"+cfg.Port, nil); err != nil {
			panic(err)
		}
	} else if cfg.Mode == "rproxy" {
		http.HandleFunc("/", rproxy)
		if err := http.ListenAndServe(cfg.Host+":"+cfg.Port, nil); err != nil {
			panic(err)
		}
	} else if cfg.Mode == "https" {
		http.Handle("/", http.FileServer(http.Dir(cfg.Src)))
		if err := http.ListenAndServeTLS(cfg.Host+":"+cfg.Port, cfg.TlsCrt, cfg.TlsKey, nil); err != nil {
			panic(err)
		}
	} else {
		log.Fatal("ERROR: Unkown mode \"" + cfg.Mode + "\"")
	}
}

func rproxy(w http.ResponseWriter, r *http.Request) {
	var u *url.URL
	for _, value := range cfg.Hosts {
		if value.Domain == r.Host {
			u2, err := url.Parse(value.Target)
			if err != nil {
				panic(err)
			}
			u = u2
		}
	}
	if u == nil {
		w.Write([]byte("Sorry that domain was not found on this server."))
	} else {
		httputil.NewSingleHostReverseProxy(u).ServeHTTP(w, r)
	}
}

func fileExist(file string) {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		fmt.Println("ERROR: File \"", file, "\" does not exist.")
		os.Exit(1)
	}
}
