# Gserve

Gserve is a small http/https/reverse proxy written in go.

## Installation

To install simply run:
```
go get gitlab.com/jacobprosser8/gserve
```

## Usage

Gserve servers are defined in small and simple configuration files written in toml, yaml or json. Simply call gserve with the desired config files as an arguments:
```
gserve myawesomeserver.toml myotherawesomeserver.toml ...
```
and your server will be launched. The following are example configurations for different server types.

### Http
``` toml
mode = "http"
host = "0.0.0.0"
src = "./"
port = "80"

```
Mode specifies whether the file is defining an http, https or reverse proxy server. Host specifies the host of the server `0.0.0.0` is used to publish the site to the internet to keep it local you can use `127.0.0.1`. Src defines the directory of static files to be served. Finally, port specifies the port the server is to listen on.

### Https
``` toml
mode = "https"
host = "0.0.0.0"
src = "./"
port = "443"
tlscrt = "server.crt"
tlskey = "server.key"
```
Tlscrt specifies the server certificate file also known as `fullchain.pem`. Tlskey specifies the server key file also known as `privkey.pem`.

### Reverse Proxy
``` toml
mode = "rproxy"
port = "80"
host  = "0.0.0.0"

[hosts]
  [hosts.one]
  domain = "myawesomedomain.com"
  target = "8080"

  [hosts.two]
  domain = "myevenaweseomerdomain.com"
  target = "9090"
```
Hosts defines each domain to be redirected, where domain specifies the domain and target specifies where it will be redirected to. In this example `myawesomedomain.com` will be redirected to port `8080` where its corresponding server will be listening and `myevenaweseomerdomain.com` will be redirected to `9090`.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## TODO
* CGI support
  * FastCGI Support
* Proxy
* ~~Allow multiple configurations to be launched in one command~~

## License
[AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)
